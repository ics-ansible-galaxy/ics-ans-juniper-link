# ics-ans-juniper-link

Ansible playbook to add and configure link aggregations on juniper switches.

## License

BSD 2-clause
