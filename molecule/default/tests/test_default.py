import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('juniper_link')


def test_lag_interface_exist(host):
    assert os.system("zcat /config/juniper.conf.gz | grep ae33")
